## CI/CD ашиглаагүй үед гар аргаар ЭХЛҮҮЛЭХ
```
sudo docker-compose up --build --remove-orphans -d
```

## CI/CD ашиглаагүй үед гар аргаар ХААЖ УСТГАХ
```
sudo docker-compose down --remove-orphans --volumes --rmi local
```